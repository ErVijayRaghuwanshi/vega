#include "stdlib.h"
#include "config.h"
#include "led.h"
#include "gpio.h"


void main ()
{
	US sw_status = 0;
	UI i = 0;
	printf("\n\r Please connet Motor to GPIO_0 and GPIO_2");
	while(1)
	{
		//Turn ON Clockwise direction and after a delay Turn OFF
		GPIO_write_pin(0,LOW);
		GPIO_write_pin(2,HIGH);
		for(i=0; i< 0x800000;i++); // Delay
		udelay(10000);
		GPIO_write_pin(0,LOW);
		GPIO_write_pin(2,LOW);

		//Turn ON Anticlockwise direction after a delay Turn OFF
		GPIO_write_pin(0,HIGH);
		GPIO_write_pin(2,LOW);
		for(i=0; i< 0x800000;i++); // Delay
		GPIO_write_pin(0,LOW);
		GPIO_write_pin(2,LOW);

		//Turn ON Clockwise direction and after a delay Turn OFF then rotate in Anticlockwise direction then Turn OFF
		GPIO_write_pin(0,LOW);
		GPIO_write_pin(2,HIGH);
		for(i=0; i< 0x800000;i++); // Delay
		GPIO_write_pin(0,HIGH);
		GPIO_write_pin(2,LOW);
		for(i=0; i< 0x800000;i++); // Delay
		udelay(10000);
		
		//Stop
		GPIO_write_pin(0,HIGH);
		GPIO_write_pin(2,HIGH);

		
	}
	
}




