# Example Programs on VEGA SoCs

|Example Programs | |
|:-----------:|----|
|Hello World|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/uart/hello_world)|
|LDR Sensor Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/ldr_sensor_pgm)|
|Relay Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/relay_pgm)|
|DS18B20 Temperature Sensor Demo |[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/gpio_1-wire/ds18b20_temperature_sensor)|
|ESP8266 WiFi Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/uart/wifi_demo)|
|HC-05 Bluetooth Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/uart/bluetooth_demo)|
|GPS6MV2 GPS Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/uart/gps_demo)|
|SIM800A GSM Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/uart/gsm_iot_sms_call_demo)|
|I2C SSD1306 OLED Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/i2c/i2c_display)|
|I2C DS3231 RTC Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/i2c/RTC_DS3231)|
|SPI Demo Program|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/spi/spi_write_read_pgm)|
|ADC Sample Program|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/adc/adc_sample_pgm)|
|LED Brightness Using PWM|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/pwm/pwm_led_brightness)|
|Proximity Sensor Program|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/proximity_sensor_pgm)|
|HC-SR04 Ultrasound Sensor Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/ultrasound_sensor_pgm)|
|PIR Motion Sensor Module Demo|[Code](https://gitlab.com/cdac-vega/vega-sdk/-/tree/master/examples/gpio/PIR_motion_sensor)|

#### and many more... Explore directories for more Programs.
